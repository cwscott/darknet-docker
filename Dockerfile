FROM daisukekobayashi/darknet:cpu-noopt-cv

RUN apt-get update && apt-get install -y sudo

# Shamelessly stolen from http://fabiorehm.com/blog/2014/09/11/running-gui-apps-with-docker/
ARG BUILD_UID
ARG BUILD_GID

RUN export uid=$BUILD_UID gid=$BUILD_GID && \
    mkdir -p /home/developer && \
    echo "developer:x:${uid}:${gid}:Developer,,,:/home/developer:/bin/bash" >> /etc/passwd && \
    echo "developer:x:${uid}:" >> /etc/group && \
    mkdir -p /etc/sudoers.d && \
    echo "developer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/developer && \
    chmod 0440 /etc/sudoers.d/developer && \
    chown ${uid}:${gid} -R /home/developer && \
    usermod -a -G video developer

USER developer
ENV HOME /home/developer
