IMAGE_NAME := darknet
DOCKER_FILE := Dockerfile

DOCKER_X11_OPTS := -e DISPLAY=$$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix
DOCKER_DARKNET_OPTS := -v $$PWD/workspace:/workspace -w /workspace
DOCKER_WEBCAM_OPTS := $(foreach webcam,$(WEBCAMS),--device=$(webcam):$(webcam))

# Variables for setting up the developer user with the same UID/GID for X11 forwarding.
CURRENT_UID := $(shell id -u)
CURRENT_GID := $(shell id -g)

all: shell

build: $(DOCKER_FILE)
	docker build -t $(IMAGE_NAME) \
	--build-arg BUILD_UID=$(CURRENT_UID) \
	--build-arg BUILD_GID=$(CURRENT_GID) \
	-f $(DOCKER_FILE) .

shell: build
	docker run -ti --rm \
	$(DOCKER_EXTRA_OPTS) \
	$(DOCKER_X11_OPTS) \
	$(DOCKER_DARKNET_OPTS) \
	$(DOCKER_WEBCAM_OPTS) \
	$(IMAGE_NAME) \
	bash

nvidia: DOCKER_EXTRA_OPTS=--runtime=nvidia
nvidia: DOCKER_FILE=Dockerfile.nvidia
nvidia: shell
