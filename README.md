# darknet-docker

This respository holds the tools needed to get up and runninng with [darknet](https://pjreddie.com/darknet/). In order to allow anyone to easily get this working, it uses a Docker container which can automatically build darknet and download all required files.

## Dependencies
- Docker ([ubuntu setup](https://docs.docker.com/engine/install/ubuntu/))
- [OPTIONAL] [nvidia-docker](https://github.com/NVIDIA/nvidia-docker) for Nvidia GPU acceleration

## Usage

To build and run the darknet container with CPU acceleration, simply run `make`.

If you want to run with Nvidia GPU acceleration, run `make nvidia`.

The `./workspace` directory will be created if it does not already exist and will be mounted in the container at `/workspace`.

Once you have a shell, you can run all the examples from the [darknet website](https://pjreddie.com/darknet/), omiting the `./` at the beginning of commands (darknet is installed globally in these containers).

### Running YOLO on a sample image

First, start the container with `make`. This will build and start the darknet container.

Next, we need to download the weights and demo data from darknet. This can be done simply by cloning the darknet repository.

```sh
git clone https://github.com/pjreddie/darknet
```

Then, we can navigate into this directory and download the weights for YOLOv3.

```sh
cd darknet
curl -O https://pjreddie.com/media/files/yolov3.weights
```

Finally, we can run the demo.

```sh
darknet detect cfg/yolov3.cfg yolov3.weights data/dog.jpg
```

This should open a window with what YOLO found.

### Running YOLO on a webcam using a Nvidia GPU

In order to pass a webcam in, we will use the `WEBCAM` variable provided in the Makefile. For this example, we will assume this is `/dev/video0` but it can be any camera device. This variable can also be provided multiple devices separated by spaces.

With this variable, we can build the Nvidia-specific version of the container.

```sh
make WEBCAMS="/dev/video0" nvidia
```

Next, download the darknet demo and YOLOv3 weights as shown above.

```sh
git clone https://github.com/pjreddie/darknet
cd darknet
curl -O https://pjreddie.com/media/files/yolov3.weights
```

We can now start darknet in webcam mode (`-c` specifies which webcam to use).

```sh
darknet detector demo cfg/coco.data cfg/yolov3.cfg yolov3.weights -c 0
```

You should see a window come up with the live detection from YOLO.

## Common issues

Below are several common issues when using this container. If you find more feel free to add them below.

### `Video-stream stopped!` when using in webcam mode

If you see this, OpenCV could not find the webcam specified. Check to be sure that the webcam chosen with the `-c` exists at `/dev/video*`.

This may also be caused by a permissions issue. Be sure the `developer` user is in the `video` group or try running with `sudo` (NOTE: This might cause issues with X11 forwarding).

### `Unable to init server: Could not connect: Connection refused`

This error is caused by the X11 forwarding failing.

The first thing to check is that the `UID` environment variable inside the container matches your `UID` outside. The container is built with your user id as the id of the user `developer` in order to make uid-mapping easier.

This can also be caused by your terminal's `DISPLAY` being incorrect or X11 server permissions being more restrictive than usual.

## Further links

Below are some links which may help when troubleshooting

- [darknet webpage](https://pjreddie.com/darknet)
- [darknet docker](https://hub.docker.com/r/daisukekobayashi/darknet)
